Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: clapper
Source: https://github.com/Rafostar/clapper

Files: *
Copyright: 2020-2022 Rafał Dzięgiel <rafostar.github@gmail.com>
License: GPL-3+

Files: lib/gst/*
Copyright: 2020-2022 Rafał Dzięgiel <rafostar.github@gmail.com>
           2018 GStreamer developers
           2015-2016 Matthew Waters <matthew@centricular.com>
           2015 Brijesh Singh <brijesh.ksingh@gmail.com>
           2015 Thibault Saunier <tsaunier@gnome.org>
           2014-2015 Sebastian Dröge <sebastian@centricular.com>
License: LGPL-2+

Files: data/com.github.rafostar.Clapper.metainfo.xml
Copyright: 2022 Rafał Dzięgiel
License: CC0-1.0

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.


License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the
 Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-3'.

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all
 copyright and related and neighboring rights to this software to the public
 domain worldwide. This software is distributed without any warranty.
 .
 On Debian systems, the complete text of the CC0 license, version 1.0,
 can be found in /usr/share/common-licenses/CC0-1.0.
