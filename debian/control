Source: clapper
Build-Depends:
 appstream-util,
 cmake,
 debhelper-compat (= 13),
 gjs,
 gobject-introspection,
 libgstreamer-plugins-base1.0-dev,
 libgstreamer1.0-dev,
 libgtk-4-dev,
 meson,
Maintainer: Johannes Schauer Marin Rodrigues <josch@debian.org>
Standards-Version: 4.6.1.0
Priority: optional
Section: misc
Vcs-Browser: https://salsa.debian.org/gnome-team/clapper
Vcs-Git: https://salsa.debian.org/gnome-team/clapper.git
Homepage: https://rafostar.github.io/clapper
Rules-Requires-Root: no

Package: clapper
Architecture: any
Depends:
 gir1.2-adw-1,
 gir1.2-gst-plugins-base-1.0,
 gir1.2-gstreamer-1.0,
 gir1.2-gtk-4.0,
 gjs,
 gstreamer1.0-plugins-good,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 gstreamer1.0-libav,
 gstreamer1.0-plugins-bad,
Description: Simple and modern GNOME media player
 Clapper is a GNOME media player built using GJS with GTK4 toolkit.
 The media player is using GStreamer as a media backend and renders
 everything via OpenGL. Player works natively on both Xorg and Wayland.
 It also supports hardware acceleration through VA-API on AMD/Intel GPUs,
 NVDEC on Nvidia and V4L2 on mobile devices.
 .
 The media player has an adaptive GUI. When viewing videos in "Windowed Mode",
 Clapper will use mostly unmodified GTK widgets to match your OS look nicely.
 When player enters "Fullscreen Mode" all GUI elements will become darker,
 bigger and semi-transparent for your viewing comfort. It also has a "Floating
 Mode" which displays only video on top of all other windows for a PiP-like
 viewing experience. Mobile friendly transitions are also supported.
